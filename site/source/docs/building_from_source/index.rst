.. _installing-from-source:

======================================================================================
Building Emscripten from Source (under-construction) 
======================================================================================

Instead of using the SDK, you can grab the code and dependencies yourself. This is a little more technical but lets you use the very latest development code. 

The instructions for setting up Emscripten on each platform are given below, followed by guidance on how to validate your environment once it is complete:

.. toctree::
   :maxdepth: 1
   
   toolchain_what_is_needed
   building_emscripten_from_source_on_linux
   building_emscripten_from_source_on_windows
   building_emscripten_from_source_on_mac_os_x
   LLVM-Backend
   verify_emscripten_environment


